﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Temporitzador
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Temporitzador))
        Me.Taulell = New System.Windows.Forms.PictureBox()
        Me.Torre_negra1 = New System.Windows.Forms.PictureBox()
        Me.Torre_negra2 = New System.Windows.Forms.PictureBox()
        Me.Cavall_negre1 = New System.Windows.Forms.PictureBox()
        Me.Cavall_negre2 = New System.Windows.Forms.PictureBox()
        Me.Alfil_negre1 = New System.Windows.Forms.PictureBox()
        Me.Alfil_negre2 = New System.Windows.Forms.PictureBox()
        Me.Reina_negra = New System.Windows.Forms.PictureBox()
        Me.Rei_negre = New System.Windows.Forms.PictureBox()
        Me.Peo_blanc1 = New System.Windows.Forms.PictureBox()
        Me.Peo_blanc7 = New System.Windows.Forms.PictureBox()
        Me.Peo_blanc6 = New System.Windows.Forms.PictureBox()
        Me.Peo_blanc5 = New System.Windows.Forms.PictureBox()
        Me.Peo_blanc4 = New System.Windows.Forms.PictureBox()
        Me.Peo_blanc3 = New System.Windows.Forms.PictureBox()
        Me.Peo_blanc2 = New System.Windows.Forms.PictureBox()
        Me.Peo_blanc8 = New System.Windows.Forms.PictureBox()
        Me.Torre_blanca1 = New System.Windows.Forms.PictureBox()
        Me.Torre_blanca2 = New System.Windows.Forms.PictureBox()
        Me.Cavall_blanc1 = New System.Windows.Forms.PictureBox()
        Me.Cavall_blanc2 = New System.Windows.Forms.PictureBox()
        Me.Alfil_blanc1 = New System.Windows.Forms.PictureBox()
        Me.Alfil_blanc2 = New System.Windows.Forms.PictureBox()
        Me.Reina_blanca = New System.Windows.Forms.PictureBox()
        Me.Rei_blanc = New System.Windows.Forms.PictureBox()
        Me.C8 = New System.Windows.Forms.PictureBox()
        Me.C7 = New System.Windows.Forms.PictureBox()
        Me.C6 = New System.Windows.Forms.PictureBox()
        Me.C5 = New System.Windows.Forms.PictureBox()
        Me.C4 = New System.Windows.Forms.PictureBox()
        Me.C3 = New System.Windows.Forms.PictureBox()
        Me.C2 = New System.Windows.Forms.PictureBox()
        Me.C1 = New System.Windows.Forms.PictureBox()
        Me.C9 = New System.Windows.Forms.PictureBox()
        Me.C10 = New System.Windows.Forms.PictureBox()
        Me.C11 = New System.Windows.Forms.PictureBox()
        Me.C12 = New System.Windows.Forms.PictureBox()
        Me.C13 = New System.Windows.Forms.PictureBox()
        Me.C14 = New System.Windows.Forms.PictureBox()
        Me.C15 = New System.Windows.Forms.PictureBox()
        Me.C16 = New System.Windows.Forms.PictureBox()
        Me.C57 = New System.Windows.Forms.PictureBox()
        Me.C58 = New System.Windows.Forms.PictureBox()
        Me.C59 = New System.Windows.Forms.PictureBox()
        Me.C60 = New System.Windows.Forms.PictureBox()
        Me.C61 = New System.Windows.Forms.PictureBox()
        Me.C62 = New System.Windows.Forms.PictureBox()
        Me.C63 = New System.Windows.Forms.PictureBox()
        Me.C64 = New System.Windows.Forms.PictureBox()
        Me.C56 = New System.Windows.Forms.PictureBox()
        Me.C55 = New System.Windows.Forms.PictureBox()
        Me.C54 = New System.Windows.Forms.PictureBox()
        Me.C53 = New System.Windows.Forms.PictureBox()
        Me.C52 = New System.Windows.Forms.PictureBox()
        Me.C51 = New System.Windows.Forms.PictureBox()
        Me.C50 = New System.Windows.Forms.PictureBox()
        Me.C49 = New System.Windows.Forms.PictureBox()
        Me.C41 = New System.Windows.Forms.PictureBox()
        Me.C42 = New System.Windows.Forms.PictureBox()
        Me.C43 = New System.Windows.Forms.PictureBox()
        Me.C44 = New System.Windows.Forms.PictureBox()
        Me.C45 = New System.Windows.Forms.PictureBox()
        Me.C46 = New System.Windows.Forms.PictureBox()
        Me.C48 = New System.Windows.Forms.PictureBox()
        Me.C47 = New System.Windows.Forms.PictureBox()
        Me.C40 = New System.Windows.Forms.PictureBox()
        Me.C39 = New System.Windows.Forms.PictureBox()
        Me.C38 = New System.Windows.Forms.PictureBox()
        Me.C37 = New System.Windows.Forms.PictureBox()
        Me.C36 = New System.Windows.Forms.PictureBox()
        Me.C35 = New System.Windows.Forms.PictureBox()
        Me.C34 = New System.Windows.Forms.PictureBox()
        Me.C33 = New System.Windows.Forms.PictureBox()
        Me.C25 = New System.Windows.Forms.PictureBox()
        Me.C26 = New System.Windows.Forms.PictureBox()
        Me.C27 = New System.Windows.Forms.PictureBox()
        Me.C28 = New System.Windows.Forms.PictureBox()
        Me.C29 = New System.Windows.Forms.PictureBox()
        Me.C30 = New System.Windows.Forms.PictureBox()
        Me.C31 = New System.Windows.Forms.PictureBox()
        Me.C32 = New System.Windows.Forms.PictureBox()
        Me.C24 = New System.Windows.Forms.PictureBox()
        Me.C23 = New System.Windows.Forms.PictureBox()
        Me.C22 = New System.Windows.Forms.PictureBox()
        Me.C21 = New System.Windows.Forms.PictureBox()
        Me.C20 = New System.Windows.Forms.PictureBox()
        Me.C19 = New System.Windows.Forms.PictureBox()
        Me.C18 = New System.Windows.Forms.PictureBox()
        Me.C17 = New System.Windows.Forms.PictureBox()
        Me.Peo_negre1 = New System.Windows.Forms.PictureBox()
        Me.Peo_negre6 = New System.Windows.Forms.PictureBox()
        Me.Peo_negre5 = New System.Windows.Forms.PictureBox()
        Me.Peo_negre4 = New System.Windows.Forms.PictureBox()
        Me.Peo_negre3 = New System.Windows.Forms.PictureBox()
        Me.Peo_negre2 = New System.Windows.Forms.PictureBox()
        Me.Peo_negre8 = New System.Windows.Forms.PictureBox()
        Me.Peo_negre7 = New System.Windows.Forms.PictureBox()
        Me.Jugador_Negre = New System.Windows.Forms.Button()
        Me.Jugador_Blanc = New System.Windows.Forms.Button()
        Me.Jugador = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.I = New System.Windows.Forms.Button()
        Me.Contador = New System.Windows.Forms.Timer(Me.components)
        CType(Me.Taulell, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Torre_negra1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Torre_negra2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cavall_negre1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cavall_negre2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alfil_negre1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alfil_negre2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Reina_negra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Rei_negre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_blanc1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_blanc7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_blanc6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_blanc5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_blanc4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_blanc3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_blanc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_blanc8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Torre_blanca1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Torre_blanca2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cavall_blanc1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cavall_blanc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alfil_blanc1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Alfil_blanc2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Reina_blanca, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Rei_blanc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.C17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_negre1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_negre6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_negre5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_negre4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_negre3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_negre2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_negre8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Peo_negre7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Taulell
        '
        Me.Taulell.Image = CType(resources.GetObject("Taulell.Image"), System.Drawing.Image)
        Me.Taulell.Location = New System.Drawing.Point(5, 5)
        Me.Taulell.Name = "Taulell"
        Me.Taulell.Size = New System.Drawing.Size(408, 407)
        Me.Taulell.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Taulell.TabIndex = 0
        Me.Taulell.TabStop = False
        '
        'Torre_negra1
        '
        Me.Torre_negra1.Image = CType(resources.GetObject("Torre_negra1.Image"), System.Drawing.Image)
        Me.Torre_negra1.Location = New System.Drawing.Point(14, 19)
        Me.Torre_negra1.Name = "Torre_negra1"
        Me.Torre_negra1.Size = New System.Drawing.Size(39, 44)
        Me.Torre_negra1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Torre_negra1.TabIndex = 73
        Me.Torre_negra1.TabStop = False
        Me.Torre_negra1.Tag = "25"
        '
        'Torre_negra2
        '
        Me.Torre_negra2.Image = CType(resources.GetObject("Torre_negra2.Image"), System.Drawing.Image)
        Me.Torre_negra2.Location = New System.Drawing.Point(357, 22)
        Me.Torre_negra2.Name = "Torre_negra2"
        Me.Torre_negra2.Size = New System.Drawing.Size(39, 42)
        Me.Torre_negra2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Torre_negra2.TabIndex = 74
        Me.Torre_negra2.TabStop = False
        Me.Torre_negra2.Tag = "32"
        '
        'Cavall_negre1
        '
        Me.Cavall_negre1.Image = CType(resources.GetObject("Cavall_negre1.Image"), System.Drawing.Image)
        Me.Cavall_negre1.Location = New System.Drawing.Point(66, 22)
        Me.Cavall_negre1.Name = "Cavall_negre1"
        Me.Cavall_negre1.Size = New System.Drawing.Size(34, 40)
        Me.Cavall_negre1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Cavall_negre1.TabIndex = 75
        Me.Cavall_negre1.TabStop = False
        Me.Cavall_negre1.Tag = "26"
        '
        'Cavall_negre2
        '
        Me.Cavall_negre2.Image = CType(resources.GetObject("Cavall_negre2.Image"), System.Drawing.Image)
        Me.Cavall_negre2.Location = New System.Drawing.Point(313, 21)
        Me.Cavall_negre2.Name = "Cavall_negre2"
        Me.Cavall_negre2.Size = New System.Drawing.Size(34, 40)
        Me.Cavall_negre2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Cavall_negre2.TabIndex = 76
        Me.Cavall_negre2.TabStop = False
        Me.Cavall_negre2.Tag = "31"
        '
        'Alfil_negre1
        '
        Me.Alfil_negre1.Image = CType(resources.GetObject("Alfil_negre1.Image"), System.Drawing.Image)
        Me.Alfil_negre1.Location = New System.Drawing.Point(115, 18)
        Me.Alfil_negre1.Name = "Alfil_negre1"
        Me.Alfil_negre1.Size = New System.Drawing.Size(37, 43)
        Me.Alfil_negre1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alfil_negre1.TabIndex = 77
        Me.Alfil_negre1.TabStop = False
        Me.Alfil_negre1.Tag = "27"
        '
        'Alfil_negre2
        '
        Me.Alfil_negre2.Image = CType(resources.GetObject("Alfil_negre2.Image"), System.Drawing.Image)
        Me.Alfil_negre2.Location = New System.Drawing.Point(263, 20)
        Me.Alfil_negre2.Name = "Alfil_negre2"
        Me.Alfil_negre2.Size = New System.Drawing.Size(37, 43)
        Me.Alfil_negre2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alfil_negre2.TabIndex = 78
        Me.Alfil_negre2.TabStop = False
        Me.Alfil_negre2.Tag = "30"
        '
        'Reina_negra
        '
        Me.Reina_negra.Image = CType(resources.GetObject("Reina_negra.Image"), System.Drawing.Image)
        Me.Reina_negra.Location = New System.Drawing.Point(168, 19)
        Me.Reina_negra.Name = "Reina_negra"
        Me.Reina_negra.Size = New System.Drawing.Size(36, 39)
        Me.Reina_negra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Reina_negra.TabIndex = 79
        Me.Reina_negra.TabStop = False
        Me.Reina_negra.Tag = "28"
        '
        'Rei_negre
        '
        Me.Rei_negre.Image = CType(resources.GetObject("Rei_negre.Image"), System.Drawing.Image)
        Me.Rei_negre.Location = New System.Drawing.Point(214, 19)
        Me.Rei_negre.Name = "Rei_negre"
        Me.Rei_negre.Size = New System.Drawing.Size(35, 39)
        Me.Rei_negre.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Rei_negre.TabIndex = 80
        Me.Rei_negre.TabStop = False
        Me.Rei_negre.Tag = "29"
        '
        'Peo_blanc1
        '
        Me.Peo_blanc1.Image = CType(resources.GetObject("Peo_blanc1.Image"), System.Drawing.Image)
        Me.Peo_blanc1.Location = New System.Drawing.Point(22, 307)
        Me.Peo_blanc1.Name = "Peo_blanc1"
        Me.Peo_blanc1.Size = New System.Drawing.Size(36, 38)
        Me.Peo_blanc1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_blanc1.TabIndex = 81
        Me.Peo_blanc1.TabStop = False
        Me.Peo_blanc1.Tag = "1"
        '
        'Peo_blanc7
        '
        Me.Peo_blanc7.Image = CType(resources.GetObject("Peo_blanc7.Image"), System.Drawing.Image)
        Me.Peo_blanc7.Location = New System.Drawing.Point(313, 309)
        Me.Peo_blanc7.Name = "Peo_blanc7"
        Me.Peo_blanc7.Size = New System.Drawing.Size(36, 38)
        Me.Peo_blanc7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_blanc7.TabIndex = 82
        Me.Peo_blanc7.TabStop = False
        Me.Peo_blanc7.Tag = "7"
        '
        'Peo_blanc6
        '
        Me.Peo_blanc6.Image = CType(resources.GetObject("Peo_blanc6.Image"), System.Drawing.Image)
        Me.Peo_blanc6.Location = New System.Drawing.Point(264, 308)
        Me.Peo_blanc6.Name = "Peo_blanc6"
        Me.Peo_blanc6.Size = New System.Drawing.Size(36, 38)
        Me.Peo_blanc6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_blanc6.TabIndex = 83
        Me.Peo_blanc6.TabStop = False
        Me.Peo_blanc6.Tag = "6"
        '
        'Peo_blanc5
        '
        Me.Peo_blanc5.Image = CType(resources.GetObject("Peo_blanc5.Image"), System.Drawing.Image)
        Me.Peo_blanc5.Location = New System.Drawing.Point(217, 309)
        Me.Peo_blanc5.Name = "Peo_blanc5"
        Me.Peo_blanc5.Size = New System.Drawing.Size(36, 38)
        Me.Peo_blanc5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_blanc5.TabIndex = 84
        Me.Peo_blanc5.TabStop = False
        Me.Peo_blanc5.Tag = "5"
        '
        'Peo_blanc4
        '
        Me.Peo_blanc4.Image = CType(resources.GetObject("Peo_blanc4.Image"), System.Drawing.Image)
        Me.Peo_blanc4.Location = New System.Drawing.Point(168, 311)
        Me.Peo_blanc4.Name = "Peo_blanc4"
        Me.Peo_blanc4.Size = New System.Drawing.Size(36, 38)
        Me.Peo_blanc4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_blanc4.TabIndex = 85
        Me.Peo_blanc4.TabStop = False
        Me.Peo_blanc4.Tag = "4"
        '
        'Peo_blanc3
        '
        Me.Peo_blanc3.Image = CType(resources.GetObject("Peo_blanc3.Image"), System.Drawing.Image)
        Me.Peo_blanc3.Location = New System.Drawing.Point(121, 308)
        Me.Peo_blanc3.Name = "Peo_blanc3"
        Me.Peo_blanc3.Size = New System.Drawing.Size(36, 38)
        Me.Peo_blanc3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_blanc3.TabIndex = 86
        Me.Peo_blanc3.TabStop = False
        Me.Peo_blanc3.Tag = "3"
        '
        'Peo_blanc2
        '
        Me.Peo_blanc2.Image = CType(resources.GetObject("Peo_blanc2.Image"), System.Drawing.Image)
        Me.Peo_blanc2.Location = New System.Drawing.Point(75, 308)
        Me.Peo_blanc2.Name = "Peo_blanc2"
        Me.Peo_blanc2.Size = New System.Drawing.Size(36, 38)
        Me.Peo_blanc2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_blanc2.TabIndex = 87
        Me.Peo_blanc2.TabStop = False
        Me.Peo_blanc2.Tag = "2"
        '
        'Peo_blanc8
        '
        Me.Peo_blanc8.Image = CType(resources.GetObject("Peo_blanc8.Image"), System.Drawing.Image)
        Me.Peo_blanc8.Location = New System.Drawing.Point(364, 309)
        Me.Peo_blanc8.Name = "Peo_blanc8"
        Me.Peo_blanc8.Size = New System.Drawing.Size(36, 37)
        Me.Peo_blanc8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_blanc8.TabIndex = 88
        Me.Peo_blanc8.TabStop = False
        Me.Peo_blanc8.Tag = "8"
        '
        'Torre_blanca1
        '
        Me.Torre_blanca1.Image = CType(resources.GetObject("Torre_blanca1.Image"), System.Drawing.Image)
        Me.Torre_blanca1.Location = New System.Drawing.Point(24, 355)
        Me.Torre_blanca1.Name = "Torre_blanca1"
        Me.Torre_blanca1.Size = New System.Drawing.Size(36, 43)
        Me.Torre_blanca1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Torre_blanca1.TabIndex = 89
        Me.Torre_blanca1.TabStop = False
        Me.Torre_blanca1.Tag = "9"
        '
        'Torre_blanca2
        '
        Me.Torre_blanca2.Image = CType(resources.GetObject("Torre_blanca2.Image"), System.Drawing.Image)
        Me.Torre_blanca2.Location = New System.Drawing.Point(363, 356)
        Me.Torre_blanca2.Name = "Torre_blanca2"
        Me.Torre_blanca2.Size = New System.Drawing.Size(36, 43)
        Me.Torre_blanca2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Torre_blanca2.TabIndex = 90
        Me.Torre_blanca2.TabStop = False
        Me.Torre_blanca2.Tag = "16"
        '
        'Cavall_blanc1
        '
        Me.Cavall_blanc1.Image = CType(resources.GetObject("Cavall_blanc1.Image"), System.Drawing.Image)
        Me.Cavall_blanc1.Location = New System.Drawing.Point(75, 355)
        Me.Cavall_blanc1.Name = "Cavall_blanc1"
        Me.Cavall_blanc1.Size = New System.Drawing.Size(31, 39)
        Me.Cavall_blanc1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Cavall_blanc1.TabIndex = 91
        Me.Cavall_blanc1.TabStop = False
        Me.Cavall_blanc1.Tag = "10"
        '
        'Cavall_blanc2
        '
        Me.Cavall_blanc2.Image = CType(resources.GetObject("Cavall_blanc2.Image"), System.Drawing.Image)
        Me.Cavall_blanc2.Location = New System.Drawing.Point(315, 357)
        Me.Cavall_blanc2.Name = "Cavall_blanc2"
        Me.Cavall_blanc2.Size = New System.Drawing.Size(31, 39)
        Me.Cavall_blanc2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Cavall_blanc2.TabIndex = 92
        Me.Cavall_blanc2.TabStop = False
        Me.Cavall_blanc2.Tag = "15"
        '
        'Alfil_blanc1
        '
        Me.Alfil_blanc1.Image = CType(resources.GetObject("Alfil_blanc1.Image"), System.Drawing.Image)
        Me.Alfil_blanc1.Location = New System.Drawing.Point(122, 355)
        Me.Alfil_blanc1.Name = "Alfil_blanc1"
        Me.Alfil_blanc1.Size = New System.Drawing.Size(32, 43)
        Me.Alfil_blanc1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alfil_blanc1.TabIndex = 93
        Me.Alfil_blanc1.TabStop = False
        Me.Alfil_blanc1.Tag = "11"
        '
        'Alfil_blanc2
        '
        Me.Alfil_blanc2.Image = CType(resources.GetObject("Alfil_blanc2.Image"), System.Drawing.Image)
        Me.Alfil_blanc2.Location = New System.Drawing.Point(266, 358)
        Me.Alfil_blanc2.Name = "Alfil_blanc2"
        Me.Alfil_blanc2.Size = New System.Drawing.Size(32, 40)
        Me.Alfil_blanc2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Alfil_blanc2.TabIndex = 94
        Me.Alfil_blanc2.TabStop = False
        Me.Alfil_blanc2.Tag = "14"
        '
        'Reina_blanca
        '
        Me.Reina_blanca.Image = CType(resources.GetObject("Reina_blanca.Image"), System.Drawing.Image)
        Me.Reina_blanca.Location = New System.Drawing.Point(170, 357)
        Me.Reina_blanca.Name = "Reina_blanca"
        Me.Reina_blanca.Size = New System.Drawing.Size(34, 38)
        Me.Reina_blanca.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Reina_blanca.TabIndex = 95
        Me.Reina_blanca.TabStop = False
        Me.Reina_blanca.Tag = "12"
        '
        'Rei_blanc
        '
        Me.Rei_blanc.Image = CType(resources.GetObject("Rei_blanc.Image"), System.Drawing.Image)
        Me.Rei_blanc.Location = New System.Drawing.Point(218, 356)
        Me.Rei_blanc.Name = "Rei_blanc"
        Me.Rei_blanc.Size = New System.Drawing.Size(34, 39)
        Me.Rei_blanc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Rei_blanc.TabIndex = 96
        Me.Rei_blanc.TabStop = False
        Me.Rei_blanc.Tag = "13"
        '
        'C8
        '
        Me.C8.Location = New System.Drawing.Point(355, 19)
        Me.C8.Name = "C8"
        Me.C8.Size = New System.Drawing.Size(44, 44)
        Me.C8.TabIndex = 8
        Me.C8.TabStop = False
        Me.C8.Tag = "32"
        '
        'C7
        '
        Me.C7.Location = New System.Drawing.Point(310, 19)
        Me.C7.Name = "C7"
        Me.C7.Size = New System.Drawing.Size(41, 41)
        Me.C7.TabIndex = 7
        Me.C7.TabStop = False
        Me.C7.Tag = "31"
        '
        'C6
        '
        Me.C6.Location = New System.Drawing.Point(255, 19)
        Me.C6.Name = "C6"
        Me.C6.Size = New System.Drawing.Size(48, 43)
        Me.C6.TabIndex = 6
        Me.C6.TabStop = False
        Me.C6.Tag = "30"
        '
        'C5
        '
        Me.C5.Location = New System.Drawing.Point(210, 19)
        Me.C5.Name = "C5"
        Me.C5.Size = New System.Drawing.Size(45, 43)
        Me.C5.TabIndex = 5
        Me.C5.TabStop = False
        Me.C5.Tag = "29"
        '
        'C4
        '
        Me.C4.Location = New System.Drawing.Point(163, 19)
        Me.C4.Name = "C4"
        Me.C4.Size = New System.Drawing.Size(47, 42)
        Me.C4.TabIndex = 4
        Me.C4.TabStop = False
        Me.C4.Tag = "28"
        '
        'C3
        '
        Me.C3.Location = New System.Drawing.Point(110, 19)
        Me.C3.Name = "C3"
        Me.C3.Size = New System.Drawing.Size(44, 42)
        Me.C3.TabIndex = 3
        Me.C3.TabStop = False
        Me.C3.Tag = "27"
        '
        'C2
        '
        Me.C2.Location = New System.Drawing.Point(60, 19)
        Me.C2.Name = "C2"
        Me.C2.Size = New System.Drawing.Size(47, 43)
        Me.C2.TabIndex = 2
        Me.C2.TabStop = False
        Me.C2.Tag = "26"
        '
        'C1
        '
        Me.C1.Location = New System.Drawing.Point(14, 19)
        Me.C1.Name = "C1"
        Me.C1.Size = New System.Drawing.Size(46, 44)
        Me.C1.TabIndex = 1
        Me.C1.TabStop = False
        Me.C1.Tag = "25"
        '
        'C9
        '
        Me.C9.Location = New System.Drawing.Point(20, 68)
        Me.C9.Name = "C9"
        Me.C9.Size = New System.Drawing.Size(43, 43)
        Me.C9.TabIndex = 9
        Me.C9.TabStop = False
        Me.C9.Tag = "17"
        '
        'C10
        '
        Me.C10.Location = New System.Drawing.Point(67, 67)
        Me.C10.Name = "C10"
        Me.C10.Size = New System.Drawing.Size(44, 41)
        Me.C10.TabIndex = 10
        Me.C10.TabStop = False
        Me.C10.Tag = "18"
        '
        'C11
        '
        Me.C11.Location = New System.Drawing.Point(113, 68)
        Me.C11.Name = "C11"
        Me.C11.Size = New System.Drawing.Size(44, 42)
        Me.C11.TabIndex = 11
        Me.C11.TabStop = False
        '
        'C12
        '
        Me.C12.Location = New System.Drawing.Point(162, 67)
        Me.C12.Name = "C12"
        Me.C12.Size = New System.Drawing.Size(44, 43)
        Me.C12.TabIndex = 12
        Me.C12.TabStop = False
        Me.C12.Tag = "20"
        '
        'C13
        '
        Me.C13.Location = New System.Drawing.Point(212, 67)
        Me.C13.Name = "C13"
        Me.C13.Size = New System.Drawing.Size(42, 41)
        Me.C13.TabIndex = 13
        Me.C13.TabStop = False
        Me.C13.Tag = "21"
        '
        'C14
        '
        Me.C14.Location = New System.Drawing.Point(260, 67)
        Me.C14.Name = "C14"
        Me.C14.Size = New System.Drawing.Size(40, 40)
        Me.C14.TabIndex = 14
        Me.C14.TabStop = False
        Me.C14.Tag = "22"
        '
        'C15
        '
        Me.C15.Location = New System.Drawing.Point(306, 67)
        Me.C15.Name = "C15"
        Me.C15.Size = New System.Drawing.Size(47, 42)
        Me.C15.TabIndex = 15
        Me.C15.TabStop = False
        Me.C15.Tag = "23"
        '
        'C16
        '
        Me.C16.Location = New System.Drawing.Point(357, 66)
        Me.C16.Name = "C16"
        Me.C16.Size = New System.Drawing.Size(43, 46)
        Me.C16.TabIndex = 16
        Me.C16.TabStop = False
        Me.C16.Tag = "24"
        '
        'C57
        '
        Me.C57.Location = New System.Drawing.Point(20, 345)
        Me.C57.Name = "C57"
        Me.C57.Size = New System.Drawing.Size(43, 46)
        Me.C57.TabIndex = 57
        Me.C57.TabStop = False
        Me.C57.Tag = "9"
        '
        'C58
        '
        Me.C58.Location = New System.Drawing.Point(70, 350)
        Me.C58.Name = "C58"
        Me.C58.Size = New System.Drawing.Size(39, 42)
        Me.C58.TabIndex = 58
        Me.C58.TabStop = False
        Me.C58.Tag = "10"
        '
        'C59
        '
        Me.C59.Location = New System.Drawing.Point(119, 350)
        Me.C59.Name = "C59"
        Me.C59.Size = New System.Drawing.Size(40, 43)
        Me.C59.TabIndex = 59
        Me.C59.TabStop = False
        Me.C59.Tag = "11"
        '
        'C60
        '
        Me.C60.Location = New System.Drawing.Point(167, 355)
        Me.C60.Name = "C60"
        Me.C60.Size = New System.Drawing.Size(42, 40)
        Me.C60.TabIndex = 60
        Me.C60.TabStop = False
        Me.C60.Tag = "12"
        '
        'C61
        '
        Me.C61.Location = New System.Drawing.Point(213, 355)
        Me.C61.Name = "C61"
        Me.C61.Size = New System.Drawing.Size(42, 40)
        Me.C61.TabIndex = 61
        Me.C61.TabStop = False
        Me.C61.Tag = "13"
        '
        'C62
        '
        Me.C62.Location = New System.Drawing.Point(262, 355)
        Me.C62.Name = "C62"
        Me.C62.Size = New System.Drawing.Size(43, 39)
        Me.C62.TabIndex = 62
        Me.C62.TabStop = False
        Me.C62.Tag = "14"
        '
        'C63
        '
        Me.C63.Location = New System.Drawing.Point(310, 355)
        Me.C63.Name = "C63"
        Me.C63.Size = New System.Drawing.Size(43, 42)
        Me.C63.TabIndex = 63
        Me.C63.TabStop = False
        Me.C63.Tag = "15"
        '
        'C64
        '
        Me.C64.Location = New System.Drawing.Point(360, 355)
        Me.C64.Name = "C64"
        Me.C64.Size = New System.Drawing.Size(41, 44)
        Me.C64.TabIndex = 64
        Me.C64.TabStop = False
        Me.C64.Tag = "16"
        '
        'C56
        '
        Me.C56.Location = New System.Drawing.Point(360, 308)
        Me.C56.Name = "C56"
        Me.C56.Size = New System.Drawing.Size(41, 40)
        Me.C56.TabIndex = 56
        Me.C56.TabStop = False
        Me.C56.Tag = "8"
        '
        'C55
        '
        Me.C55.Location = New System.Drawing.Point(309, 307)
        Me.C55.Name = "C55"
        Me.C55.Size = New System.Drawing.Size(45, 40)
        Me.C55.TabIndex = 55
        Me.C55.TabStop = False
        Me.C55.Tag = "7"
        '
        'C54
        '
        Me.C54.Location = New System.Drawing.Point(260, 305)
        Me.C54.Name = "C54"
        Me.C54.Size = New System.Drawing.Size(43, 41)
        Me.C54.TabIndex = 54
        Me.C54.TabStop = False
        Me.C54.Tag = "6"
        '
        'C53
        '
        Me.C53.Location = New System.Drawing.Point(214, 308)
        Me.C53.Name = "C53"
        Me.C53.Size = New System.Drawing.Size(43, 41)
        Me.C53.TabIndex = 53
        Me.C53.TabStop = False
        Me.C53.Tag = "5"
        '
        'C52
        '
        Me.C52.Location = New System.Drawing.Point(163, 310)
        Me.C52.Name = "C52"
        Me.C52.Size = New System.Drawing.Size(45, 39)
        Me.C52.TabIndex = 52
        Me.C52.TabStop = False
        Me.C52.Tag = "4"
        '
        'C51
        '
        Me.C51.Location = New System.Drawing.Point(117, 307)
        Me.C51.Name = "C51"
        Me.C51.Size = New System.Drawing.Size(41, 39)
        Me.C51.TabIndex = 51
        Me.C51.TabStop = False
        Me.C51.Tag = "3"
        '
        'C50
        '
        Me.C50.Location = New System.Drawing.Point(70, 305)
        Me.C50.Name = "C50"
        Me.C50.Size = New System.Drawing.Size(43, 41)
        Me.C50.TabIndex = 50
        Me.C50.TabStop = False
        Me.C50.Tag = "2"
        '
        'C49
        '
        Me.C49.Location = New System.Drawing.Point(20, 305)
        Me.C49.Name = "C49"
        Me.C49.Size = New System.Drawing.Size(42, 42)
        Me.C49.TabIndex = 49
        Me.C49.TabStop = False
        Me.C49.Tag = "1"
        '
        'C41
        '
        Me.C41.Location = New System.Drawing.Point(20, 260)
        Me.C41.Name = "C41"
        Me.C41.Size = New System.Drawing.Size(40, 41)
        Me.C41.TabIndex = 41
        Me.C41.TabStop = False
        Me.C41.Tag = "-1"
        '
        'C42
        '
        Me.C42.Location = New System.Drawing.Point(68, 260)
        Me.C42.Name = "C42"
        Me.C42.Size = New System.Drawing.Size(44, 41)
        Me.C42.TabIndex = 42
        Me.C42.TabStop = False
        Me.C42.Tag = "-1"
        '
        'C43
        '
        Me.C43.Location = New System.Drawing.Point(117, 260)
        Me.C43.Name = "C43"
        Me.C43.Size = New System.Drawing.Size(42, 41)
        Me.C43.TabIndex = 43
        Me.C43.TabStop = False
        Me.C43.Tag = "-1"
        '
        'C44
        '
        Me.C44.Location = New System.Drawing.Point(165, 260)
        Me.C44.Name = "C44"
        Me.C44.Size = New System.Drawing.Size(43, 42)
        Me.C44.TabIndex = 44
        Me.C44.TabStop = False
        Me.C44.Tag = "-1"
        '
        'C45
        '
        Me.C45.Location = New System.Drawing.Point(213, 260)
        Me.C45.Name = "C45"
        Me.C45.Size = New System.Drawing.Size(43, 41)
        Me.C45.TabIndex = 45
        Me.C45.TabStop = False
        Me.C45.Tag = "-1"
        '
        'C46
        '
        Me.C46.Location = New System.Drawing.Point(262, 260)
        Me.C46.Name = "C46"
        Me.C46.Size = New System.Drawing.Size(43, 39)
        Me.C46.TabIndex = 46
        Me.C46.TabStop = False
        Me.C46.Tag = "-1"
        '
        'C48
        '
        Me.C48.Location = New System.Drawing.Point(359, 260)
        Me.C48.Name = "C48"
        Me.C48.Size = New System.Drawing.Size(41, 43)
        Me.C48.TabIndex = 48
        Me.C48.TabStop = False
        Me.C48.Tag = "-1"
        '
        'C47
        '
        Me.C47.Location = New System.Drawing.Point(310, 260)
        Me.C47.Name = "C47"
        Me.C47.Size = New System.Drawing.Size(43, 41)
        Me.C47.TabIndex = 47
        Me.C47.TabStop = False
        Me.C47.Tag = "-1"
        '
        'C40
        '
        Me.C40.Location = New System.Drawing.Point(360, 212)
        Me.C40.Name = "C40"
        Me.C40.Size = New System.Drawing.Size(39, 42)
        Me.C40.TabIndex = 40
        Me.C40.TabStop = False
        Me.C40.Tag = "-1"
        '
        'C39
        '
        Me.C39.Location = New System.Drawing.Point(310, 212)
        Me.C39.Name = "C39"
        Me.C39.Size = New System.Drawing.Size(43, 42)
        Me.C39.TabIndex = 39
        Me.C39.TabStop = False
        Me.C39.Tag = "-1"
        '
        'C38
        '
        Me.C38.Location = New System.Drawing.Point(260, 212)
        Me.C38.Name = "C38"
        Me.C38.Size = New System.Drawing.Size(46, 42)
        Me.C38.TabIndex = 38
        Me.C38.TabStop = False
        Me.C38.Tag = "-1"
        '
        'C37
        '
        Me.C37.Location = New System.Drawing.Point(212, 212)
        Me.C37.Name = "C37"
        Me.C37.Size = New System.Drawing.Size(41, 38)
        Me.C37.TabIndex = 37
        Me.C37.TabStop = False
        Me.C37.Tag = "-1"
        '
        'C36
        '
        Me.C36.Location = New System.Drawing.Point(164, 212)
        Me.C36.Name = "C36"
        Me.C36.Size = New System.Drawing.Size(43, 41)
        Me.C36.TabIndex = 36
        Me.C36.TabStop = False
        Me.C36.Tag = "-1"
        '
        'C35
        '
        Me.C35.Location = New System.Drawing.Point(115, 212)
        Me.C35.Name = "C35"
        Me.C35.Size = New System.Drawing.Size(41, 39)
        Me.C35.TabIndex = 35
        Me.C35.TabStop = False
        Me.C35.Tag = "-1"
        '
        'C34
        '
        Me.C34.Location = New System.Drawing.Point(67, 212)
        Me.C34.Name = "C34"
        Me.C34.Size = New System.Drawing.Size(44, 40)
        Me.C34.TabIndex = 34
        Me.C34.TabStop = False
        Me.C34.Tag = "-1"
        '
        'C33
        '
        Me.C33.Location = New System.Drawing.Point(21, 212)
        Me.C33.Name = "C33"
        Me.C33.Size = New System.Drawing.Size(41, 41)
        Me.C33.TabIndex = 33
        Me.C33.TabStop = False
        Me.C33.Tag = "-1"
        '
        'C25
        '
        Me.C25.Location = New System.Drawing.Point(17, 160)
        Me.C25.Name = "C25"
        Me.C25.Size = New System.Drawing.Size(46, 49)
        Me.C25.TabIndex = 25
        Me.C25.TabStop = False
        Me.C25.Tag = "-1"
        '
        'C26
        '
        Me.C26.Location = New System.Drawing.Point(67, 162)
        Me.C26.Name = "C26"
        Me.C26.Size = New System.Drawing.Size(41, 45)
        Me.C26.TabIndex = 26
        Me.C26.TabStop = False
        Me.C26.Tag = "-1"
        '
        'C27
        '
        Me.C27.Location = New System.Drawing.Point(115, 162)
        Me.C27.Name = "C27"
        Me.C27.Size = New System.Drawing.Size(44, 45)
        Me.C27.TabIndex = 27
        Me.C27.TabStop = False
        Me.C27.Tag = "-1"
        '
        'C28
        '
        Me.C28.Location = New System.Drawing.Point(163, 162)
        Me.C28.Name = "C28"
        Me.C28.Size = New System.Drawing.Size(44, 44)
        Me.C28.TabIndex = 28
        Me.C28.TabStop = False
        Me.C28.Tag = "-1"
        '
        'C29
        '
        Me.C29.Location = New System.Drawing.Point(212, 162)
        Me.C29.Name = "C29"
        Me.C29.Size = New System.Drawing.Size(42, 44)
        Me.C29.TabIndex = 29
        Me.C29.TabStop = False
        Me.C29.Tag = "-1"
        '
        'C30
        '
        Me.C30.Location = New System.Drawing.Point(260, 162)
        Me.C30.Name = "C30"
        Me.C30.Size = New System.Drawing.Size(45, 42)
        Me.C30.TabIndex = 30
        Me.C30.TabStop = False
        Me.C30.Tag = "-1"
        '
        'C31
        '
        Me.C31.Location = New System.Drawing.Point(310, 162)
        Me.C31.Name = "C31"
        Me.C31.Size = New System.Drawing.Size(45, 45)
        Me.C31.TabIndex = 31
        Me.C31.TabStop = False
        Me.C31.Tag = "-1"
        '
        'C32
        '
        Me.C32.Location = New System.Drawing.Point(360, 162)
        Me.C32.Name = "C32"
        Me.C32.Size = New System.Drawing.Size(40, 41)
        Me.C32.TabIndex = 32
        Me.C32.TabStop = False
        Me.C32.Tag = "-1"
        '
        'C24
        '
        Me.C24.Location = New System.Drawing.Point(358, 117)
        Me.C24.Name = "C24"
        Me.C24.Size = New System.Drawing.Size(41, 41)
        Me.C24.TabIndex = 24
        Me.C24.TabStop = False
        Me.C24.Tag = "-1"
        '
        'C23
        '
        Me.C23.Location = New System.Drawing.Point(309, 117)
        Me.C23.Name = "C23"
        Me.C23.Size = New System.Drawing.Size(42, 43)
        Me.C23.TabIndex = 23
        Me.C23.TabStop = False
        Me.C23.Tag = "-1"
        '
        'C22
        '
        Me.C22.Location = New System.Drawing.Point(257, 117)
        Me.C22.Name = "C22"
        Me.C22.Size = New System.Drawing.Size(48, 42)
        Me.C22.TabIndex = 22
        Me.C22.TabStop = False
        Me.C22.Tag = "-1"
        '
        'C21
        '
        Me.C21.Location = New System.Drawing.Point(210, 117)
        Me.C21.Name = "C21"
        Me.C21.Size = New System.Drawing.Size(44, 43)
        Me.C21.TabIndex = 21
        Me.C21.TabStop = False
        Me.C21.Tag = "-1"
        '
        'C20
        '
        Me.C20.Location = New System.Drawing.Point(162, 117)
        Me.C20.Name = "C20"
        Me.C20.Size = New System.Drawing.Size(44, 43)
        Me.C20.TabIndex = 20
        Me.C20.TabStop = False
        Me.C20.Tag = "-1"
        '
        'C19
        '
        Me.C19.Location = New System.Drawing.Point(115, 117)
        Me.C19.Name = "C19"
        Me.C19.Size = New System.Drawing.Size(41, 41)
        Me.C19.TabIndex = 19
        Me.C19.TabStop = False
        Me.C19.Tag = "-1"
        '
        'C18
        '
        Me.C18.BackColor = System.Drawing.Color.Transparent
        Me.C18.Location = New System.Drawing.Point(68, 117)
        Me.C18.Name = "C18"
        Me.C18.Size = New System.Drawing.Size(43, 41)
        Me.C18.TabIndex = 18
        Me.C18.TabStop = False
        Me.C18.Tag = "-1"
        '
        'C17
        '
        Me.C17.BackColor = System.Drawing.Color.Transparent
        Me.C17.Location = New System.Drawing.Point(20, 117)
        Me.C17.Name = "C17"
        Me.C17.Size = New System.Drawing.Size(41, 42)
        Me.C17.TabIndex = 17
        Me.C17.TabStop = False
        Me.C17.Tag = "-1"
        '
        'Peo_negre1
        '
        Me.Peo_negre1.Image = CType(resources.GetObject("Peo_negre1.Image"), System.Drawing.Image)
        Me.Peo_negre1.Location = New System.Drawing.Point(18, 70)
        Me.Peo_negre1.Name = "Peo_negre1"
        Me.Peo_negre1.Size = New System.Drawing.Size(43, 45)
        Me.Peo_negre1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_negre1.TabIndex = 97
        Me.Peo_negre1.TabStop = False
        Me.Peo_negre1.Tag = "17"
        '
        'Peo_negre6
        '
        Me.Peo_negre6.Image = CType(resources.GetObject("Peo_negre6.Image"), System.Drawing.Image)
        Me.Peo_negre6.Location = New System.Drawing.Point(257, 65)
        Me.Peo_negre6.Name = "Peo_negre6"
        Me.Peo_negre6.Size = New System.Drawing.Size(43, 45)
        Me.Peo_negre6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_negre6.TabIndex = 98
        Me.Peo_negre6.TabStop = False
        Me.Peo_negre6.Tag = "22"
        '
        'Peo_negre5
        '
        Me.Peo_negre5.Image = CType(resources.GetObject("Peo_negre5.Image"), System.Drawing.Image)
        Me.Peo_negre5.Location = New System.Drawing.Point(210, 70)
        Me.Peo_negre5.Name = "Peo_negre5"
        Me.Peo_negre5.Size = New System.Drawing.Size(43, 45)
        Me.Peo_negre5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_negre5.TabIndex = 99
        Me.Peo_negre5.TabStop = False
        Me.Peo_negre5.Tag = "21"
        '
        'Peo_negre4
        '
        Me.Peo_negre4.Image = CType(resources.GetObject("Peo_negre4.Image"), System.Drawing.Image)
        Me.Peo_negre4.Location = New System.Drawing.Point(161, 66)
        Me.Peo_negre4.Name = "Peo_negre4"
        Me.Peo_negre4.Size = New System.Drawing.Size(43, 45)
        Me.Peo_negre4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_negre4.TabIndex = 100
        Me.Peo_negre4.TabStop = False
        Me.Peo_negre4.Tag = "20"
        '
        'Peo_negre3
        '
        Me.Peo_negre3.Image = CType(resources.GetObject("Peo_negre3.Image"), System.Drawing.Image)
        Me.Peo_negre3.Location = New System.Drawing.Point(113, 70)
        Me.Peo_negre3.Name = "Peo_negre3"
        Me.Peo_negre3.Size = New System.Drawing.Size(43, 45)
        Me.Peo_negre3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_negre3.TabIndex = 101
        Me.Peo_negre3.TabStop = False
        Me.Peo_negre3.Tag = "19"
        '
        'Peo_negre2
        '
        Me.Peo_negre2.Image = CType(resources.GetObject("Peo_negre2.Image"), System.Drawing.Image)
        Me.Peo_negre2.Location = New System.Drawing.Point(64, 70)
        Me.Peo_negre2.Name = "Peo_negre2"
        Me.Peo_negre2.Size = New System.Drawing.Size(43, 45)
        Me.Peo_negre2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_negre2.TabIndex = 102
        Me.Peo_negre2.TabStop = False
        Me.Peo_negre2.Tag = "18"
        '
        'Peo_negre8
        '
        Me.Peo_negre8.Image = CType(resources.GetObject("Peo_negre8.Image"), System.Drawing.Image)
        Me.Peo_negre8.Location = New System.Drawing.Point(358, 67)
        Me.Peo_negre8.Name = "Peo_negre8"
        Me.Peo_negre8.Size = New System.Drawing.Size(43, 45)
        Me.Peo_negre8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_negre8.TabIndex = 103
        Me.Peo_negre8.TabStop = False
        Me.Peo_negre8.Tag = "24"
        '
        'Peo_negre7
        '
        Me.Peo_negre7.Image = CType(resources.GetObject("Peo_negre7.Image"), System.Drawing.Image)
        Me.Peo_negre7.Location = New System.Drawing.Point(309, 65)
        Me.Peo_negre7.Name = "Peo_negre7"
        Me.Peo_negre7.Size = New System.Drawing.Size(43, 45)
        Me.Peo_negre7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Peo_negre7.TabIndex = 104
        Me.Peo_negre7.TabStop = False
        Me.Peo_negre7.Tag = "23"
        '
        'Jugador_Negre
        '
        Me.Jugador_Negre.Location = New System.Drawing.Point(421, 30)
        Me.Jugador_Negre.Name = "Jugador_Negre"
        Me.Jugador_Negre.Size = New System.Drawing.Size(129, 49)
        Me.Jugador_Negre.TabIndex = 105
        Me.Jugador_Negre.Text = "Jugador Negre"
        Me.Jugador_Negre.UseVisualStyleBackColor = True
        '
        'Jugador_Blanc
        '
        Me.Jugador_Blanc.Location = New System.Drawing.Point(421, 342)
        Me.Jugador_Blanc.Name = "Jugador_Blanc"
        Me.Jugador_Blanc.Size = New System.Drawing.Size(129, 49)
        Me.Jugador_Blanc.TabIndex = 106
        Me.Jugador_Blanc.Text = "Jugador Blanc"
        Me.Jugador_Blanc.UseVisualStyleBackColor = True
        '
        'Jugador
        '
        Me.Jugador.AutoSize = True
        Me.Jugador.Location = New System.Drawing.Point(430, 160)
        Me.Jugador.Name = "Jugador"
        Me.Jugador.Size = New System.Drawing.Size(120, 13)
        Me.Jugador.TabIndex = 107
        Me.Jugador.Text = "Quin jugador comença?"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(436, 198)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 108
        Me.Label1.Text = "Contador"
        '
        'I
        '
        Me.I.Location = New System.Drawing.Point(488, 195)
        Me.I.Name = "I"
        Me.I.Size = New System.Drawing.Size(44, 27)
        Me.I.TabIndex = 109
        Me.I.Text = "Iniciar"
        Me.I.UseVisualStyleBackColor = True
        '
        'Contador
        '
        Me.Contador.Interval = 1000
        '
        'Temporitzador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(785, 489)
        Me.Controls.Add(Me.I)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Jugador)
        Me.Controls.Add(Me.Jugador_Blanc)
        Me.Controls.Add(Me.Jugador_Negre)
        Me.Controls.Add(Me.Peo_negre7)
        Me.Controls.Add(Me.Peo_negre8)
        Me.Controls.Add(Me.Peo_negre2)
        Me.Controls.Add(Me.Peo_negre3)
        Me.Controls.Add(Me.Peo_negre4)
        Me.Controls.Add(Me.Peo_negre5)
        Me.Controls.Add(Me.Peo_negre6)
        Me.Controls.Add(Me.Peo_negre1)
        Me.Controls.Add(Me.Rei_blanc)
        Me.Controls.Add(Me.Reina_blanca)
        Me.Controls.Add(Me.Alfil_blanc2)
        Me.Controls.Add(Me.Alfil_blanc1)
        Me.Controls.Add(Me.Cavall_blanc2)
        Me.Controls.Add(Me.Cavall_blanc1)
        Me.Controls.Add(Me.Torre_blanca2)
        Me.Controls.Add(Me.Torre_blanca1)
        Me.Controls.Add(Me.Peo_blanc8)
        Me.Controls.Add(Me.Peo_blanc2)
        Me.Controls.Add(Me.Peo_blanc3)
        Me.Controls.Add(Me.Peo_blanc4)
        Me.Controls.Add(Me.Peo_blanc5)
        Me.Controls.Add(Me.Peo_blanc6)
        Me.Controls.Add(Me.Peo_blanc7)
        Me.Controls.Add(Me.Peo_blanc1)
        Me.Controls.Add(Me.Rei_negre)
        Me.Controls.Add(Me.Reina_negra)
        Me.Controls.Add(Me.Alfil_negre2)
        Me.Controls.Add(Me.Alfil_negre1)
        Me.Controls.Add(Me.Cavall_negre2)
        Me.Controls.Add(Me.Cavall_negre1)
        Me.Controls.Add(Me.Torre_negra2)
        Me.Controls.Add(Me.Torre_negra1)
        Me.Controls.Add(Me.C64)
        Me.Controls.Add(Me.C63)
        Me.Controls.Add(Me.C62)
        Me.Controls.Add(Me.C61)
        Me.Controls.Add(Me.C60)
        Me.Controls.Add(Me.C59)
        Me.Controls.Add(Me.C58)
        Me.Controls.Add(Me.C57)
        Me.Controls.Add(Me.C56)
        Me.Controls.Add(Me.C55)
        Me.Controls.Add(Me.C54)
        Me.Controls.Add(Me.C53)
        Me.Controls.Add(Me.C52)
        Me.Controls.Add(Me.C51)
        Me.Controls.Add(Me.C50)
        Me.Controls.Add(Me.C49)
        Me.Controls.Add(Me.C48)
        Me.Controls.Add(Me.C47)
        Me.Controls.Add(Me.C46)
        Me.Controls.Add(Me.C45)
        Me.Controls.Add(Me.C44)
        Me.Controls.Add(Me.C43)
        Me.Controls.Add(Me.C42)
        Me.Controls.Add(Me.C41)
        Me.Controls.Add(Me.C40)
        Me.Controls.Add(Me.C39)
        Me.Controls.Add(Me.C38)
        Me.Controls.Add(Me.C37)
        Me.Controls.Add(Me.C36)
        Me.Controls.Add(Me.C35)
        Me.Controls.Add(Me.C34)
        Me.Controls.Add(Me.C33)
        Me.Controls.Add(Me.C32)
        Me.Controls.Add(Me.C31)
        Me.Controls.Add(Me.C30)
        Me.Controls.Add(Me.C29)
        Me.Controls.Add(Me.C28)
        Me.Controls.Add(Me.C27)
        Me.Controls.Add(Me.C26)
        Me.Controls.Add(Me.C25)
        Me.Controls.Add(Me.C24)
        Me.Controls.Add(Me.C23)
        Me.Controls.Add(Me.C22)
        Me.Controls.Add(Me.C21)
        Me.Controls.Add(Me.C20)
        Me.Controls.Add(Me.C19)
        Me.Controls.Add(Me.C18)
        Me.Controls.Add(Me.C17)
        Me.Controls.Add(Me.C16)
        Me.Controls.Add(Me.C15)
        Me.Controls.Add(Me.C14)
        Me.Controls.Add(Me.C13)
        Me.Controls.Add(Me.C12)
        Me.Controls.Add(Me.C11)
        Me.Controls.Add(Me.C10)
        Me.Controls.Add(Me.C9)
        Me.Controls.Add(Me.C8)
        Me.Controls.Add(Me.C7)
        Me.Controls.Add(Me.C6)
        Me.Controls.Add(Me.C5)
        Me.Controls.Add(Me.C4)
        Me.Controls.Add(Me.C3)
        Me.Controls.Add(Me.C2)
        Me.Controls.Add(Me.C1)
        Me.Controls.Add(Me.Taulell)
        Me.Name = "Temporitzador"
        Me.Text = "Form1"
        CType(Me.Taulell, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Torre_negra1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Torre_negra2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cavall_negre1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cavall_negre2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alfil_negre1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alfil_negre2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Reina_negra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Rei_negre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_blanc1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_blanc7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_blanc6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_blanc5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_blanc4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_blanc3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_blanc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_blanc8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Torre_blanca1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Torre_blanca2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cavall_blanc1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cavall_blanc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alfil_blanc1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Alfil_blanc2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Reina_blanca, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Rei_blanc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.C17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_negre1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_negre6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_negre5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_negre4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_negre3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_negre2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_negre8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Peo_negre7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Taulell As PictureBox
    Friend WithEvents Torre_negra1 As PictureBox
    Friend WithEvents Torre_negra2 As PictureBox
    Friend WithEvents Cavall_negre1 As PictureBox
    Friend WithEvents Cavall_negre2 As PictureBox
    Friend WithEvents Alfil_negre1 As PictureBox
    Friend WithEvents Alfil_negre2 As PictureBox
    Friend WithEvents Reina_negra As PictureBox
    Friend WithEvents Rei_negre As PictureBox
    Friend WithEvents Peo_blanc1 As PictureBox
    Friend WithEvents Peo_blanc7 As PictureBox
    Friend WithEvents Peo_blanc6 As PictureBox
    Friend WithEvents Peo_blanc5 As PictureBox
    Friend WithEvents Peo_blanc4 As PictureBox
    Friend WithEvents Peo_blanc3 As PictureBox
    Friend WithEvents Peo_blanc2 As PictureBox
    Friend WithEvents Peo_blanc8 As PictureBox
    Friend WithEvents Torre_blanca1 As PictureBox
    Friend WithEvents Torre_blanca2 As PictureBox
    Friend WithEvents Cavall_blanc1 As PictureBox
    Friend WithEvents Cavall_blanc2 As PictureBox
    Friend WithEvents Alfil_blanc1 As PictureBox
    Friend WithEvents Alfil_blanc2 As PictureBox
    Friend WithEvents Reina_blanca As PictureBox
    Friend WithEvents Rei_blanc As PictureBox
    Friend WithEvents C8 As PictureBox
    Friend WithEvents C7 As PictureBox
    Friend WithEvents C6 As PictureBox
    Friend WithEvents C5 As PictureBox
    Friend WithEvents C4 As PictureBox
    Friend WithEvents C3 As PictureBox
    Friend WithEvents C2 As PictureBox
    Friend WithEvents C1 As PictureBox
    Friend WithEvents C9 As PictureBox
    Friend WithEvents C10 As PictureBox
    Friend WithEvents C11 As PictureBox
    Friend WithEvents C12 As PictureBox
    Friend WithEvents C13 As PictureBox
    Friend WithEvents C14 As PictureBox
    Friend WithEvents C15 As PictureBox
    Friend WithEvents C16 As PictureBox
    Friend WithEvents C57 As PictureBox
    Friend WithEvents C58 As PictureBox
    Friend WithEvents C59 As PictureBox
    Friend WithEvents C60 As PictureBox
    Friend WithEvents C61 As PictureBox
    Friend WithEvents C62 As PictureBox
    Friend WithEvents C63 As PictureBox
    Friend WithEvents C64 As PictureBox
    Friend WithEvents C56 As PictureBox
    Friend WithEvents C55 As PictureBox
    Friend WithEvents C54 As PictureBox
    Friend WithEvents C53 As PictureBox
    Friend WithEvents C52 As PictureBox
    Friend WithEvents C51 As PictureBox
    Friend WithEvents C50 As PictureBox
    Friend WithEvents C49 As PictureBox
    Friend WithEvents C41 As PictureBox
    Friend WithEvents C42 As PictureBox
    Friend WithEvents C43 As PictureBox
    Friend WithEvents C44 As PictureBox
    Friend WithEvents C45 As PictureBox
    Friend WithEvents C46 As PictureBox
    Friend WithEvents C48 As PictureBox
    Friend WithEvents C47 As PictureBox
    Friend WithEvents C40 As PictureBox
    Friend WithEvents C39 As PictureBox
    Friend WithEvents C38 As PictureBox
    Friend WithEvents C37 As PictureBox
    Friend WithEvents C36 As PictureBox
    Friend WithEvents C35 As PictureBox
    Friend WithEvents C34 As PictureBox
    Friend WithEvents C33 As PictureBox
    Friend WithEvents C25 As PictureBox
    Friend WithEvents C26 As PictureBox
    Friend WithEvents C27 As PictureBox
    Friend WithEvents C28 As PictureBox
    Friend WithEvents C29 As PictureBox
    Friend WithEvents C30 As PictureBox
    Friend WithEvents C31 As PictureBox
    Friend WithEvents C32 As PictureBox
    Friend WithEvents C24 As PictureBox
    Friend WithEvents C23 As PictureBox
    Friend WithEvents C22 As PictureBox
    Friend WithEvents C21 As PictureBox
    Friend WithEvents C20 As PictureBox
    Friend WithEvents C19 As PictureBox
    Friend WithEvents C18 As PictureBox
    Friend WithEvents C17 As PictureBox
    Friend WithEvents Peo_negre1 As PictureBox
    Friend WithEvents Peo_negre6 As PictureBox
    Friend WithEvents Peo_negre5 As PictureBox
    Friend WithEvents Peo_negre4 As PictureBox
    Friend WithEvents Peo_negre3 As PictureBox
    Friend WithEvents Peo_negre2 As PictureBox
    Friend WithEvents Peo_negre8 As PictureBox
    Friend WithEvents Peo_negre7 As PictureBox
    Friend WithEvents Jugador_Negre As Button
    Friend WithEvents Jugador_Blanc As Button
    Friend WithEvents Jugador As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents I As Button
    Friend WithEvents Contador As Timer
End Class
